#!/usr/bin/env python

import numpy as np
import cv2
import mediapipe as mp
import networkx as nx
import sys


# Read points from text file
def readPoints(path):
    # Create an array of points.
    points = [];
    # Read points
    with open(path) as file:
        for line in file:
            x, y = line.split()
            points.append((int(x), int(y)))

    return points


# Apply affine transform calculated using srcTri and dstTri to src and
# output an image of size.
def applyAffineTransform(src, srcTri, dstTri, size):
    # Given a pair of triangles, find the affine transform.
    warpMat = cv2.getAffineTransform(np.float32(srcTri), np.float32(dstTri))

    # Apply the Affine Transform just found to the src image
    dst = cv2.warpAffine(src, warpMat, (size[0], size[1]), None, flags=cv2.INTER_LINEAR,
                         borderMode=cv2.BORDER_REFLECT_101)

    return dst


# Warps and alpha blends triangular regions from img1 and img2 to img
def morphTriangle(img1, img2, img, t1, t2, t, alpha):
    # Find bounding rectangle for each triangle
    r1 = cv2.boundingRect(np.float32([t1]))
    r2 = cv2.boundingRect(np.float32([t2]))
    r = cv2.boundingRect(np.float32([t]))

    # Offset points by left top corner of the respective rectangles
    t1Rect = []
    t2Rect = []
    tRect = []

    for i in range(0, 3):
        tRect.append(((t[i][0] - r[0]), (t[i][1] - r[1])))
        t1Rect.append(((t1[i][0] - r1[0]), (t1[i][1] - r1[1])))
        t2Rect.append(((t2[i][0] - r2[0]), (t2[i][1] - r2[1])))

    # Get mask by filling triangle
    mask = np.zeros((r[3], r[2], 3), dtype=np.float32)
    cv2.fillConvexPoly(mask, np.int32(tRect), (1.0, 1.0, 1.0), 16, 0)

    # Apply warpImage to small rectangular patches
    img1Rect = img1[r1[1]:r1[1] + r1[3], r1[0]:r1[0] + r1[2]]
    img2Rect = img2[r2[1]:r2[1] + r2[3], r2[0]:r2[0] + r2[2]]

    size = (r[2], r[3])
    warpImage1 = applyAffineTransform(img1Rect, t1Rect, tRect, size)
    warpImage2 = applyAffineTransform(img2Rect, t2Rect, tRect, size)

    # Alpha blend rectangular patches
    imgRect = (1.0 - alpha) * warpImage1 + alpha * warpImage2

    # Copy triangular region of the rectangular patch to the output image
    img[r[1]:r[1] + r[3], r[0]:r[0] + r[2]] = img[r[1]:r[1] + r[3], r[0]:r[0] + r[2]] * (1 - mask) + imgRect * mask


def overlay_image_alpha(img, img_overlay, x, y, alpha_mask):
    """Overlay `img_overlay` onto `img` at (x, y) and blend using `alpha_mask`.

    `alpha_mask` must have same HxW as `img_overlay` and values in range [0, 1].
    """
    # Image ranges
    y1, y2 = max(0, y), min(img.shape[0], y + img_overlay.shape[0])
    x1, x2 = max(0, x), min(img.shape[1], x + img_overlay.shape[1])

    # Overlay ranges
    y1o, y2o = max(0, -y), min(img_overlay.shape[0], img.shape[0] - y)
    x1o, x2o = max(0, -x), min(img_overlay.shape[1], img.shape[1] - x)

    # Exit if nothing to do
    if y1 >= y2 or x1 >= x2 or y1o >= y2o or x1o >= x2o:
        return

    # Blend overlay within the determined ranges
    img_crop = img[y1:y2, x1:x2]
    img_overlay_crop = img_overlay[y1o:y2o, x1o:x2o]
    alpha = alpha_mask[y1o:y2o, x1o:x2o, np.newaxis]
    alpha_inv = 1.0 - alpha

    img_crop[:] = alpha * img_overlay_crop + alpha_inv * img_crop

def vertex_index(vertices, vertex):
    count = 0
    for v in vertices:
        if vertex[0] == v[0] and v[1] == vertex[1]:
            return count
        count += 1




def merge_faces(face1, face2):

    image1 = face1 #cv2.imread(face1)
    image2 = face2 #cv2.imread(face2)

    # INITIALIZING OBJECTS
    mp_face_mesh = mp.solutions.face_mesh


    with mp_face_mesh.FaceMesh(min_detection_confidence=0.5, min_tracking_confidence=0.5) as face_mesh:

        #get face landmarks from images, in scalar value (0, 1)
        landmarks_1 = face_mesh.process(image1)
        landmarks_2 = face_mesh.process(image2)


        #transform landmarks to a space between 0 and 1024
        landmarks_1_out = []
        landmarks_2_out = []

        rect = (0, 0, 1024, 1024)

        # Draw the face mesh annotations on the image.
        if landmarks_1.multi_face_landmarks and landmarks_2.multi_face_landmarks:

            for landmarks in landmarks_1.multi_face_landmarks:
                for lm in landmarks.landmark:
                    landmarks_1_out.append((int(lm.x * 1024), int(lm.y * 1024)))

            for landmarks in landmarks_2.multi_face_landmarks:
                for lm in landmarks.landmark:
                    landmarks_2_out.append((int(lm.x * 1024), int(lm.y * 1024)))

        alpha = 0.5
        edge_points = [(0, 0), (0, 511), (0, 1023), (511, 1023), (1023, 1023), (1023, 511), (1023, 0), (511, 0)]


        # Convert Mat to float data type
        img1 = np.float32(image1)
        img2 = np.float32(image2)

        # Read array of corresponding points
        points1 = landmarks_1_out
        points1 += edge_points

        points2 = landmarks_2_out
        points2 += edge_points

        subdiv = cv2.Subdiv2D(rect)
        subdiv.insert(points1)
        triangles = []

        for triangle in subdiv.getTriangleList():
            v0 = (triangle[0], triangle[1])
            v1 = (triangle[2], triangle[3])
            v2 = (triangle[4], triangle[5])

            triangle_idx = (vertex_index(points1, v0), vertex_index(points1, v1), vertex_index(points1, v2))
            triangles.append(triangle_idx)

        points = []
        # Compute weighted average point coordinates
        for i in range(0, len(points1)):
            x = (1 - alpha) * points1[i][0] + alpha * points2[i][0]
            y = (1 - alpha) * points1[i][1] + alpha * points2[i][1]
            points.append((x, y))

        # Allocate space for final output
        imgMorph = np.zeros(img1.shape, dtype=img1.dtype)

        # Read triangles from tri.txt
        for triangle in triangles:
            x, y, z = triangle

            x = int(x)
            y = int(y)
            z = int(z)

            t1 = [points1[x], points1[y], points1[z]]
            t2 = [points2[x], points2[y], points2[z]]
            t = [points[x], points[y], points[z]]


            # Morph one triangle at a time.
            morphTriangle(img1, img2, imgMorph, t1, t2, t, alpha)

        alpha_mask = np.ones((1024, 1024))
        overlay_image_alpha(img1, imgMorph, 0, 0, alpha_mask)

        # Display Result
        #cv2.imshow("Morphed Face", np.uint8(img1))
        #cv2.waitKey(0)

        return np.uint8(img1)


def crop_face(file, width=1024, height=1024):

    PADDING = 200

    # Read the input image
    img = cv2.imread(file)
    height, width = img.shape[:2]

    # Convert into grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Load the cascade
    face_cascade = cv2.CascadeClassifier('Classifiers/haarcascade_frontalface_default.xml')

    # Detect faces
    faces = face_cascade.detectMultiScale(gray, 1.1, 4)

    face = faces[0]

    # Draw rectangle around the faces and crop the faces
    x, y, w, h = faces[0]
    #cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
    face = img[max(0, y-PADDING): min(height, y + h + PADDING), max(0, x - PADDING): min(width, x + w + PADDING)]

    face = cv2.resize(face, (1024, 1024))
    #cv2.imshow("face", face)
    #cv2.imwrite('face.jpg', faces)


    return face

    # Display the output
    #cv2.imwrite('detcted.jpg', img)
    #cv2.imshow('img', img)
    #cv2.waitKey()


    return face

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print("Usage: python script.py bad arguments")
        sys.exit()
    playerName = sys.argv[1]
    parent01 = sys.argv[2]
    parent02 = sys.argv[3]
    parent03 = sys.argv[4]

    filename1 = 'faces/' + parent01 + '.jpg'
    filename2 = 'faces/' + parent02 + '.jpg'
    filename3 = 'faces/' + parent03 + '.jpg'
    print(filename1)
    face1 = crop_face(filename1)
    face2 = crop_face(filename2)
    face3 = crop_face(filename3)


    face4 = merge_faces(face1, face2)
    
    face5 = merge_faces(face3, face4)
    cv2.imwrite(playerName + '_son.jpg', face5)