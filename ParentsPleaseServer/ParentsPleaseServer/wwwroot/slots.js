function setupStep1() {
  const step = 1;
  const centerX = canvasWidth / 2;
  const centerY = canvasHeight / 2;
  const innerSpace = 20;
  const nineCards = [
    [centerX - innerSpace - cardWidth, centerY - innerSpace - cardHeight],
    [centerX, centerY - innerSpace - cardHeight],
    [centerX + innerSpace + cardWidth, centerY - innerSpace - cardHeight],
    [centerX - innerSpace - cardWidth, centerY],
    [centerX, centerY],
    [centerX + innerSpace + cardWidth, centerY],
    [centerX - innerSpace - cardWidth, centerY + innerSpace + cardHeight],
    [centerX, centerY + innerSpace + cardHeight],
    [centerX + innerSpace + cardWidth, centerY + innerSpace + cardHeight],
  ];
  const nAltro = [
    nineCards[0][0] - innerSpace * 0 - cardWidth * 2.5,
    nineCards[0][1],
  ];
  const nPlayer = [
    nineCards[8][0] + innerSpace * 0 + cardWidth * 2.5,
    nineCards[8][1],
  ];

  const thisSlots = [];

  thisSlots.push(
    new Slot(nAltro[0], nAltro[1], cardWidth * 0.75, cardHeight * 0.75, 0, step)
  );
  thisSlots.push(
    new Slot(nPlayer[0], nPlayer[1], cardWidth, cardHeight, 1, step)
  );

  for (let i = 0; i < 9; i++) {
    thisSlots.push(
      new Slot(
        nineCards[i][0],
        nineCards[i][1],
        cardWidth,
        cardHeight,
        i + 2,
        step
      )
    );
  }

  slots.push(thisSlots);
}

function setupStep2() {
  const step = 2;
  const centerX = canvasWidth / 2;
  const centerY = canvasHeight / 2;
  const innerSpace = 20;
  const bottomCards = [
    [centerX - innerSpace - cardWidth, centerY + innerSpace + cardHeight],
    [centerX, centerY + innerSpace + cardHeight],
    [centerX + innerSpace + cardWidth, centerY + innerSpace + cardHeight],
  ];
  const figlioProprio = [centerX, centerY - innerSpace * 2 - cardHeight / 2];

  const thisSlots = [];

  thisSlots.push(
    new Slot(
      figlioProprio[0],
      figlioProprio[1],
      cardWidth * 2.5,
      cardHeight * 2.5,
      0,
      step
    )
  );

  for (let i = 0; i < 3; i++) {
    thisSlots.push(
      new Slot(
        bottomCards[i][0],
        bottomCards[i][1],
        cardWidth,
        cardHeight,
        i + 1,
        step
      )
    );
  }

  slots.push(thisSlots);
}

function setupStep3() {
  const step = 3;
  const centerX = canvasWidth / 2;
  const centerY = canvasHeight / 2;
  const innerSpace = 20;
  const nineCards = [
    [centerX - innerSpace - cardWidth, centerY - innerSpace - cardHeight],
    [centerX, centerY - innerSpace - cardHeight],
    [centerX + innerSpace + cardWidth, centerY - innerSpace - cardHeight],
    [centerX - innerSpace - cardWidth, centerY],
    [centerX, centerY],
    [centerX + innerSpace + cardWidth, centerY],
    [centerX - innerSpace - cardWidth, centerY + innerSpace + cardHeight],
    [centerX, centerY + innerSpace + cardHeight],
    [centerX + innerSpace + cardWidth, centerY + innerSpace + cardHeight],
  ];
  const figlioAltro = [
    nineCards[3][0] - innerSpace * 0 - cardWidth * 2.2,
    nineCards[3][1],
  ];
  const nPlayer = [
    nineCards[8][0] + innerSpace * 0 + cardWidth * 2.5,
    nineCards[8][1],
  ];

  const thisSlots = [];

  thisSlots.push(
    new Slot(
      figlioAltro[0],
      figlioAltro[1],
      cardWidth * 2.5,
      cardHeight * 2.5,
      0,
      step
    )
  );
  thisSlots.push(
    new Slot(nPlayer[0], nPlayer[1], cardWidth, cardHeight, 1, step)
  );

  for (let i = 0; i < 9; i++) {
    thisSlots.push(
      new Slot(
        nineCards[i][0],
        nineCards[i][1],
        cardWidth,
        cardHeight,
        i + 2,
        step
      )
    );
  }

  slots.push(thisSlots);
}

function setupStep4() {
  const step = 4;
  const centerX = canvasWidth / 2;
  const centerY = canvasHeight / 2;
  const innerSpace = 15;

  const leftAnchor = centerX - cardWidth * 3;
  const nineCardsLeft = [
    [
      leftAnchor - innerSpace - cardWidth * 0.7,
      centerY - innerSpace - cardHeight * 0.7,
    ],
    [leftAnchor, centerY - innerSpace - cardHeight * 0.7],
    [
      leftAnchor + innerSpace + cardWidth * 0.7,
      centerY - innerSpace - cardHeight * 0.7,
    ],
    [leftAnchor - innerSpace - cardWidth * 0.7, centerY],
    [leftAnchor, centerY],
    [leftAnchor + innerSpace + cardWidth * 0.7, centerY],
    [
      leftAnchor - innerSpace - cardWidth * 0.7,
      centerY + innerSpace + cardHeight * 0.7,
    ],
    [leftAnchor, centerY + innerSpace + cardHeight * 0.7],
    [
      leftAnchor + innerSpace + cardWidth * 0.7,
      centerY + innerSpace + cardHeight * 0.7,
    ],
  ];

  const rightAnchor = centerX + cardWidth * 3;
  const nineCardsRight = [
    [
      rightAnchor - innerSpace - cardWidth * 0.7,
      centerY - innerSpace - cardHeight * 0.7,
    ],
    [rightAnchor, centerY - innerSpace - cardHeight * 0.7],
    [
      rightAnchor + innerSpace + cardWidth * 0.7,
      centerY - innerSpace - cardHeight * 0.7,
    ],
    [rightAnchor - innerSpace - cardWidth * 0.7, centerY],
    [rightAnchor, centerY],
    [rightAnchor + innerSpace + cardWidth * 0.7, centerY],
    [
      rightAnchor - innerSpace - cardWidth * 0.7,
      centerY + innerSpace + cardHeight * 0.7,
    ],
    [rightAnchor, centerY + innerSpace + cardHeight * 0.7],
    [
      rightAnchor + innerSpace + cardWidth * 0.7,
      centerY + innerSpace + cardHeight * 0.7,
    ],
  ];

  const figlioPlayer = [
    nineCardsLeft[2][0] + innerSpace * 0 + cardWidth * 1.6,
    nineCardsLeft[2][1],
  ];

  const figlioAltro = [
    nineCardsRight[6][0] + innerSpace * 0 - cardWidth * 1.6,
    nineCardsRight[6][1],
  ];

  const thisSlots = [];

  thisSlots.push(
    new Slot(
      figlioPlayer[0],
      figlioPlayer[1],
      cardWidth * 1.4,
      cardHeight * 1.4,
      0,
      step
    )
  );

  thisSlots.push(
    new Slot(
      figlioAltro[0],
      figlioAltro[1],
      cardWidth * 1.4,
      cardHeight * 1.4,
      1,
      step
    )
  );

  for (let i = 0; i < 9; i++) {
    thisSlots.push(
      new Slot(
        nineCardsLeft[i][0],
        nineCardsLeft[i][1],
        cardWidth * 0.7,
        cardHeight * 0.7,
        i + 2,
        step
      )
    );
  }

  for (let i = 0; i < 9; i++) {
    thisSlots.push(
      new Slot(
        nineCardsRight[i][0],
        nineCardsRight[i][1],
        cardWidth * 0.7,
        cardHeight * 0.7,
        i + 11,
        step
      )
    );
  }

  slots.push(thisSlots);
}

class Slot {
  constructor(x, y, width, height, id, step) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.id = id;
    this.step = step;
    this.cardId = false;
  }

  show() {
    grafici[this.step].imageMode(CENTER);
    // let img;
    // if (this.cardId !== false) {
    //   img = cards[this.cardId].image;
    // } else if (this.cardId == "mySon") {
    //   img = mySon;
    // } else {
    //   img = cards[0].image;
    // }
    // grafici[this.step].image(img, this.x, this.y, this.width, this.height);

    if (this.cardId == "mySon") {
      grafici[this.step].image(mySon, this.x, this.y, this.width, this.height);
    } else if (this.cardId == "otherSon") {
      grafici[this.step].image(
        otherSon,
        this.x,
        this.y,
        this.width,
        this.height
      );
    } else if (this.cardId !== false) {
      // console.log(cards[this.cardId]);
      // noLoop();
      grafici[this.step].image(
        cards[this.cardId].image,
        this.x,
        this.y,
        this.width,
        this.height
      );
      // } else
    } else {
      grafici[this.step].image(
        cards[0].image,
        this.x,
        this.y,
        this.width,
        this.height
      );
    }

    grafici[this.step].rectMode(CENTER);
    grafici[this.step].strokeWeight(3);
    grafici[this.step].stroke(50);
    grafici[this.step].noFill();
    grafici[this.step].rect(this.x, this.y, this.width, this.height, round);
  }

  click() {}
}

function mousePressed() {
  if (step > 0) {
    for (let i = 0; i < slots[step - 1].length; i++) {
      const left = slots[step - 1][i].x - slots[step - 1][i].width / 2;
      const right = slots[step - 1][i].x + slots[step - 1][i].width / 2;
      const top = slots[step - 1][i].y - slots[step - 1][i].height / 2;
      const bottom = slots[step - 1][i].y + slots[step - 1][i].height / 2;

      if (Number.isInteger(slots[step - 1][i].cardId)) {
        if (
          mouseX > left &&
          mouseX < right &&
          mouseY > top &&
          mouseY < bottom
        ) {
          console.log("Clicked: " + slots[step - 1][i].cardId);
          switch (step) {
            case 1:
              if (chosenThree.length < 3) {
                if(!chosenThree.includes(slots[step - 1][i].cardId)){
                  chosenThree.push(slots[step - 1][i].cardId);
                  console.log(chosenThree);
                }
              }
              if (chosenThree.length == 3) {
                getMySon(chosenThree);
                setTimeout(goToStep2, 2000);
              }
              break;
            case 3:
              if (chosenThree.length < 3) {
                if(!chosenThree.includes(slots[step - 1][i].cardId)){
                  chosenThree.push(slots[step - 1][i].cardId);
                  console.log(chosenThree);
                }
              }
              if (chosenThree.length == 3) {
                getMySon(chosenThree);
                setTimeout(goToStep4, 2000);
              }
              break;
            // case 4:

            // break;
          }
        }
      }
    }
  }
}

function hover() {
  if (step > 0) {
    for (let i = 0; i < slots[step - 1].length; i++) {
      const left = slots[step - 1][i].x - slots[step - 1][i].width / 2;
      const right = slots[step - 1][i].x + slots[step - 1][i].width / 2;
      const top = slots[step - 1][i].y - slots[step - 1][i].height / 2;
      const bottom = slots[step - 1][i].y + slots[step - 1][i].height / 2;

      if (slots[step - 1][i].cardId !== false) {
        if (
          mouseX > left &&
          mouseX < right &&
          mouseY > top &&
          mouseY < bottom
        ) {
          console.log("Hovering: " + slots[step - 1][i].cardId);
        }
      }
    }
  }
}

function getMySon(chosenThree) {
  chosenThreeString = "";

  for (let i = 0; i < 3; i++) {
    chosenThreeString += "/";
    if (chosenThree[i] < 10) {
      chosenThreeString += "0";
    }
    chosenThreeString += chosenThree[i];
  }

  const myImage = new Image();

  myImage.onload = function () {
    mySon = loadImage(
      "http://192.168.137.253:5127/api/Game/GetSon/player1" + chosenThreeString
    );
  };

  mySon = loadImage("http://192.168.137.253:5127/api/Game/GetSon/player1" + chosenThreeString);
  console.log("http://192.168.137.253:5127/api/Game/GetSon/player1" + chosenThreeString);

  myImage.src =
    "http://192.168.137.253:5127/api/Game/GetSon/player1" + chosenThreeString;
}
