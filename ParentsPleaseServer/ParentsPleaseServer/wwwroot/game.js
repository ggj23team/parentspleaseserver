const canvasWidth = 1024;
const canvasHeight = 768;

const round = 5;
const cardRatio = 2.5 / 3.5;
const cardWidth = 100;
const cardHeight = cardWidth / cardRatio;

const grafici = [];
let step = 0;
const slots = [];

function setup() {
  frameRate(30);
  createCanvas(canvasWidth, canvasHeight);
  rectMode(CENTER);
  for (let i = 0; i < 5; i++) {
    grafici.push(createGraphics(canvasWidth, canvasHeight));
  }
  //   setupStep0();
  setupStep1();
  setupStep2();
  setupStep3();
  setupStep4();
  // noLoop();
}

function draw() {
  background(120, 220, 120);
  for (let i = 0; i < slots.length; i++) {
    for (let j = 0; j < slots[i].length; j++) {
      slots[i][j].show();
    }
  }

   if (frameCount % 30 == 0) {
     update();
   }


   fetch("http://192.168.137.253:5127/api/Game/imready/player"+playerNumber)

  switch (gameStatus) {
    case "WaitingPlayers":
      step = 0;
      break;
    case "SetupGame":
      step = 0;
      break;
    case "ChooseParents":
      step = 1;
      break;
    case "WaitOtherPlayersChild":
      step = 1;
      break;
    case "DownloadOtherChiild":
      step = 1;
      break;
    case "WaitingPlayersGuess":
      step = 2;
      break;
    case "SolveTurn":
      step = 3;
      break;
    case "NewTurn":
      step = 1;
      break;
    case "GameOver":
      step = 0;
      break;
  }

  image(grafici[step], 0, 0);
  // hover();
}

function keyPressed() {
  if (keyCode == 37) {
    step--;
  } else if (keyCode == 39) {
    step++;
  } else if (keyCode == 40) {
    console.log("Avviata richiesta dei mazzi");
    getShuffleDeck();
  } else if (keyCode == 38) {
    goToStep1();
  }

  if (step == 5) {
    step = 0;
  } else if (step == -1) {
    step = 4;
  }

  // console.log(step);
  clear();
  redraw();

  //   grafici[0].ellipse(random(width), random(height), 50);
}

function getShuffleDeck() {
  fetch("http://192.168.137.253:5127/api/Game/GetShuffleDeck/player"+playerNumber+"/", {
    method: "GET",
    // mode: 'no-cors'
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
    })
    .catch((error) => {
      console.error("Si è verificato un errore:", error);
    });
}
