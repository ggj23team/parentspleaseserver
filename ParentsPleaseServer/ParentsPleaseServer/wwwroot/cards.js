const cards = [];
const names = [
  "Nome 1",
  "Nome 2",
  "Nome 3",
  "Nome 4",
  "Nome 5",
  "Nome 6",
  "Nome 7",
  "Nome 8",
  "Nome 9",
  "Nome 10",
  "Nome 11",
  "Nome 12",
  "Nome 13",
  "Nome 14",
  "Nome 15",
  "Nome 16",
  "Nome 17",
  "Nome 18",
  "Nome 19",
  "Nome 20",
  "Nome 21",
  "Nome 22",
  "Nome 23",
  "Nome 24",
  "Nome 25",
  "Nome 26",
];

// DECK FINTI PER PROVE AUTONOME
const player1 = [4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 0, 1, 2, 3];
const player2 = [14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26];
const chosenThree = [];

let mySon;
let otherSon;

let gameStatus;

function preload() {
  mySon = loadImage("images/mySon.jpg"); // Rimuovere quando funzionerà il figlio dal server
  otherSon = loadImage("images/otherSon.jpg"); // Rimuovere quando funzionerà il figlio dal server

  const img = loadImage("images/back.png");
  cards.push(new Card("back", img, "back"));
  for (let i = 0; i < 26; i++) {
    const img = loadImage("images/face (" + (i + 1) + ").png"); // Aggiornare poi numero di carta/file
    cards.push(new Card(i, img, names[i]));
  }
}

class Card {
  constructor(id, image, name) {
    // this.x;
    // this.y;
    this.id = id;
    this.image = image;
    this.name = name;
    this.player;
  }
}

function goToStep1() {
  step = 1;
  chosenThree.length = 0;
  for (let i = 0; i < 9; i++) {
    // console.log(slots[step-1][i]);
    slots[step - 1][i + 2].cardId = player1[i]; // Aggiornare poi numero di carta/file
    // console.log(slots[step - 1][i + 2].cardId); // Aggiornare poi numero di carta/file
    // console.log(slots[step - 1][i + 2].cardId);
  }
  // console.log(slots[step - 1]);
  // console.log(cards);
}

function goToStep2() {
  // console.log("Step 2");
  step = 2;
  slots[step - 1][0].cardId = "mySon";
  for (let i = 0; i < 3; i++) {
    slots[step - 1][i + 1].cardId = chosenThree[i]; // Aggiornare poi numero di carta/file
    // console.log(slots[step - 1][i + 1].cardId);
  }

  console.log(slots[step - 1]);
  // console.log(cards);

  setTimeout(goToStep3, 6000);
}

function goToStep3() {
  step = 3;
  chosenThree.length = 0;
  slots[step - 1][0].cardId = "otherSon";

  for (let i = 0; i < 9; i++) {
    slots[step - 1][i + 2].cardId = player2[i]; // Aggiornare poi numero di carta/file
  }
}

function goToStep4() {
  step = 4;
  console.log("Step 4 (risultato). Ancora da fare.");
  // for (let i = 0; i < 9; i++) {
  //   slots[step - 1][i + 2].cardId = player2[i]; // Aggiornare poi numero di carta/file
  // }

  setTimeout(goToStep1, 6000);
}
