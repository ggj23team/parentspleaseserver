﻿namespace ParentsPleaseServer.Model
{

    public enum GameStatus
    {
        WaitingPlayers,
        SetupGame,
        DownloadOtherDeck,
        ChooseParents,
        WaitOtherPlayersChild,
        DownloadOtherChiild,
        WaitingPlayersGuess,
        GetOtherAnswer,
        SolveTurn,
        NewTurn,
        GameOver
    }
    public class Game
    {
        public GameStatus status { get; set; }

        public Player player1 = new Player();
        public Player player2 = new Player();

        public Game()
        {
            status = GameStatus.WaitingPlayers;
        }

        public Player GetPlayer(string playerName)
        {
            if (playerName.Equals("player1"))
                return player1;
            return player2;
        }

        public Player GetOtherPlayer(string playerName)
        {
            if (playerName.Equals("player1"))
                return player2;
            return player1;
        }

        public bool PlayersAreReady()
        {
            return player1.isReady && player2.isReady;
        }

        public bool PlayersGotDeck()
        {
            return player1.hasDeck && player2 .hasDeck;
        }


        public void ResetGame()
        {
            player1 = new Player();
            player2 = new Player();
            status = GameStatus.WaitingPlayers;
        }

        public string Update()
        {
            switch(status)
            {
                case GameStatus.WaitingPlayers:
                    if (PlayersAreReady())
                    {
                        status = GameStatus.SetupGame;
                    }
                    break;
                case GameStatus.SetupGame:
                    if (PlayersGotDeck())
                    {
                        status = GameStatus.DownloadOtherDeck;
                    }
                    break;
                case GameStatus.DownloadOtherDeck:
                    if(player1.hasOtherDeck && player2.hasOtherDeck)
                    {
                        status = GameStatus.ChooseParents;
                    }
                    break;
                case GameStatus.ChooseParents:
                    if (player1.sonIsReady || player2.sonIsReady)
                        status = GameStatus.WaitOtherPlayersChild;
                    break;
                case GameStatus.WaitOtherPlayersChild:
                    if (player1.sonIsReady && player2.sonIsReady)
                        status = GameStatus.DownloadOtherChiild;
                    break;
                case GameStatus.DownloadOtherChiild:
                    if (player1.DownloadedOtherPhoto && player2.DownloadedOtherPhoto)
                        status = GameStatus.WaitingPlayersGuess;
                    break;
                case GameStatus.WaitingPlayersGuess:
                    if(player1.guessSent && player2.guessSent)
                        status = GameStatus.GetOtherAnswer;
                    break;
                case GameStatus.GetOtherAnswer:
                    if (player1.answerReceived && player2.answerReceived)
                        status = GameStatus.SolveTurn;
                    break;
                case GameStatus.SolveTurn:
                    if(player1.IsAlive() && player2.IsAlive())
                    {
                        status = GameStatus.NewTurn;
                    }

                    else
                    {
                        status = GameStatus.GameOver;
                    }
                    break;
                case GameStatus.NewTurn:
                    player1.ResetTurn();
                    player2.ResetTurn();
                    status = GameStatus.ChooseParents;
                    break;
                case GameStatus.GameOver:
                    player1 = new Player();
                    player2 = new Player();
                    status = GameStatus.WaitingPlayers;
                    break;
                default:
                    break;
            }

            return status.ToString();
        }
    }
}
