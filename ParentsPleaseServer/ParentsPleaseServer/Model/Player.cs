﻿namespace ParentsPleaseServer.Model
{
    public class Player
    {
        public bool isReady { get; set; }
        public bool hasDeck { get; set; }

        public bool hasOtherDeck { get; set; }

        public string[] deck { get; set; }

        public bool sonIsReady { get; set; }
        public bool DownloadedOtherPhoto { get; set; }

        public bool guessSent { get; set; }

        public bool answerReceived { get; set; }

        public List<string> rightAnswers { get; set; }

        public int life = 26 - 9;



        public HashSet<string> lastParents;

        public Player()
        {
            isReady = false;
            hasDeck = false;
            hasOtherDeck = false;
            sonIsReady = false;
            DownloadedOtherPhoto = false;
            guessSent = false;
            answerReceived = false;
            deck = new string[26];
            lastParents = new HashSet<string>();
            rightAnswers = new List<string>();
            life = 26 - 9;
        }

        public void SetLastParents(string parent01, string parent02, string parent03)
        {
            lastParents.Clear();
            lastParents.Add(parent01);
            lastParents.Add(parent02);
            lastParents.Add(parent03);
        }

        public List<string> CheckMyguess(string parent01, string parent02, string parent03)
        {
            rightAnswers = new List<string>();

            if (lastParents.Contains(parent01))
            {
                rightAnswers.Add(parent01);
                life--;
            }

            if (lastParents.Contains(parent02))
            {
                rightAnswers.Add(parent02);
                life--;
            }

            if (lastParents.Contains(parent03))
            {
                rightAnswers.Add(parent03);
                life--;
            }

            return rightAnswers;
        }

        public void ResetTurn()
        {
            sonIsReady = false;
            DownloadedOtherPhoto = false;
            guessSent = false;
            answerReceived = false;
            lastParents.Clear();
        }

        public bool IsAlive()
        {
            return life > 0;
        }

    }
}
