using System.IO;
using Microsoft.AspNetCore.Mvc;
using ParentsPleaseServer.Controllers.Utilities;
using ParentsPleaseServer.Model;
using System.Diagnostics;
using System.Net;

namespace ParentsPleaseServer.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class GameController : ControllerBase
    {
        private static Game game = new Game();
        static string[] cardsNames = { "01", "02", "03", "04", "05", "06", "07", "08", "09",
            "10","11", "12", "13", "14", "15", "16", "17", "18", "19", "20","21",
            "22", "23", "24", "25", "26" };
        private readonly ILogger<GameController> _logger;

        public GameController(ILogger<GameController> logger)
        {
            _logger = logger;
        }


        [HttpGet("update/{playerName}")]
        public ActionResult<IEnumerable<string>> Update(string playerName)
        {
            Player currentPlayer = game.GetPlayer(playerName);
            return Ok(new ResponseContainer<string>() { gameStatus = game.Update(), Response = "daje" });
        }

        [HttpGet("GetSon/{playerName}/{parent01}/{parent02}/{parent03}")]
        public ActionResult GetSon(string playerName, string parent01, string parent02, string parent03)
        {

            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "python",
                    Arguments = "pythonStuff/createSon.py " + playerName + " " + parent01 + " " + parent02 + " " + parent03,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };

            process.Start();
            string output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            var image = System.IO.File.OpenRead(playerName + "_son.jpg");
            Player currentPlayer = game.GetPlayer(playerName);
            currentPlayer.sonIsReady = true;
            currentPlayer.SetLastParents(parent01, parent02, parent03);
            game.Update();
            return File(image, "image/jpeg");
        }

        [HttpGet("DownloadOtherSon/{playerName}")]
        public ActionResult DownloadOtherSon(string playerName)
        {
            Player currentPlayer = game.GetPlayer(playerName);
            currentPlayer.DownloadedOtherPhoto = true;
            if(playerName == "player1")
            {
                var image02 = System.IO.File.OpenRead("player2" + "_son.jpg");
                return File(image02, "image/jpeg");
            }
            var image01 = System.IO.File.OpenRead("player1" + "_son.jpg");
            return File(image01, "image/jpeg");
        }


            private static void Shuffle(string[] array)
        {
            Random rng = new Random();
            int n = array.Length;
            while (n > 1)
            {
                int k = rng.Next(n--);
                string temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }
        }

        [HttpGet("GetShuffleDeck/{playerName}")]
        public ActionResult<IEnumerable<string>> GetShuffleDeck(string playerName)
        {
            Player currentPlayer = game.GetPlayer(playerName);
            currentPlayer.hasDeck = true;
            Shuffle(cardsNames);
            Array.Copy(cardsNames, currentPlayer.deck, 26);
            return Ok(new ResponseContainer<string[]>() { gameStatus = game.Update(), Response = cardsNames });
        }


        [HttpGet("GetOtherDeck/{playerName}")]
        public ActionResult<IEnumerable<string>> GetOtherDeck(string playerName)
        {
            Player otherPlayer = game.GetOtherPlayer(playerName);
            game.GetPlayer(playerName).hasOtherDeck = true;
            return Ok(new ResponseContainer<string[]>() { gameStatus = game.Update(), Response = otherPlayer.deck });
        }

        [HttpGet("imready/{playerName}")]
        public ActionResult<IEnumerable<string>> IAmReady(string playerName)
        {
            Player currentPlayer = game.GetPlayer(playerName);
            currentPlayer.isReady = true;
            return Ok(new ResponseContainer<string>() { gameStatus = game.Update(), Response = "daje" });
        }


        [HttpGet("CheckMyGuess/{playerName}/{parent01}/{parent02}/{parent03}")]
        public ActionResult CheckMyGuess(string playerName, string parent01, string parent02, string parent03)
        {
            Player otherPlayer = game.GetOtherPlayer(playerName);
            Player currentPlayer = game.GetPlayer(playerName);
            List<string> answers = otherPlayer.CheckMyguess(parent01, parent02, parent03);
            currentPlayer.guessSent = true;
            return Ok(new ResponseContainer<List<string>>() { gameStatus = game.Update(), Response = answers });
        }

        [HttpGet("GetOtherAnswers/{playerName}")]
        public ActionResult GetOtherAnswers(string playerName)
        {
            Player currentPlayer = game.GetPlayer(playerName);
            currentPlayer.answerReceived = true;
            List<string> answers = currentPlayer.rightAnswers;

            return Ok(new ResponseContainer<List<string>>() { gameStatus = game.Update(), Response = answers });
        }
    }
   
}