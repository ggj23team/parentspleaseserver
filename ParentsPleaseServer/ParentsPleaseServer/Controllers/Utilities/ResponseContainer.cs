﻿using ParentsPleaseServer.Model;

namespace ParentsPleaseServer.Controllers.Utilities
{
    public class ResponseContainer<T>
    {
        public string gameStatus { get; set; }
        public T Response { get; set; }
        public string Error { get; set; }

        public ResponseContainer(T Response)
        {
            this.Response = Response;
        }

        public ResponseContainer()
        {

        }
    }
}
